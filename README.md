## Object-oriented programming - OOP `Xping` Problem 

`Xping, S.A. de C.V.` wants to solve next problems:

```python
class Cat:
    pass 
```
```python
fido = Cat()
fido
```
```  
<__main__.Cat at 0x111a83be0>
```

```python
fido.miau()
```

### Problem 1

```python
---------------------------------------------------------------------------

AttributeError                            Traceback (most recent call last)

<ipython-input-3-d177611622ff> in <module>
----> 1 fido.miau()


AttributeError: 'Cat' object has no attribute 'miau'

```
### Problem 2

```python

new_fido = Cat()
new_fido.miau()
---------------------------------------------------------------------------

TypeError                                 Traceback (most recent call last)

<ipython-input-9-6e1d350549b7> in <module>
      1 new_fido = Cat()
----> 2 new_fido.miau()


TypeError: miau() takes 0 positional arguments but 1 was given

``` 

### Problem 3

Something is wrong, test must be `True`.

```python
"""driver code"""

# Let's create fido
fido = Cat()
print("1. ", fido.miau() == "I'm going to Miau this time. Miau...!")
print("2. ", fido.who_am_i() == fido)

# Let's create another fido
new_fido = Cat()
print("3. ", new_fido.who_am_i() == new_fido)

fido.doble_miau = make_a_miau
print("4. ", fido.doble_miau() == "Miau... Miau")


``` 
